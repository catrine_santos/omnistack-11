
/**
 * 
 * Rota/recurso
 * 
 * Métodos HTTP:
 * 
 * GET: Buscar
 * POST: Criar
 * PUT: Alterar
 * DELETE: Deletar
 * 
 * Tipos de parâmetros:
 * 
 * Query Params: Parâmetros nomeados enviados na rota após ? (para filtros e paginação)
 * Route Params: Parâmetros utilizados para identificar recursos 
 * Request Body: Corpo da requisição, utilizado para criar e alterar recursos 
 * 
 * install nodemon -D para atualização automática do servidor.
 * -D salva como dependencia de desenvolvimento.
 * no script de package.json  "start": "nodemon arquivo..."
 * 
 */
const express = require('express'); 
const routes = require('./routes');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);


app.listen(3333); 