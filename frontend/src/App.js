import React from 'react';
//import Logon from './pages/Logon'
import './global.css';

import Routes from './routes';

function App() {
  return (
    <Routes/>
    );
  }
  export default App;
  
  /** 
   * Import {useState} from 'react'
   * const [counter, setCounter] = useState(0)
   * setCounter(counter +1) para sobrescrever o valor original de counter
   */